# Shop test application

This is a sample application featuring some Symfony functionalities. 
On "Products" page user can create, update or delete a product. Similary, categories can be created, updated or deleted on "Category" page. A product can have one category. These actions can be also made using an Admin panel. You can also register a new user.

To work with products or categories user must be logged in as administrator. A regular user can only browse products and categories.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

1. Symfony
2. XAMPP (if on Windows)


### Installing

* [Symfony](https://symfony.com/doc/2.8/setup.html)

*On Linux and macOs*
```
$ sudo mkdir -p /usr/local/bin
$ sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
$ sudo chmod a+x /usr/local/bin/symfony
```

*On Windows*

```
c:\> php -r "file_put_contents('symfony', file_get_contents('https://symfony.com/installer'));"

c:\> copy symfony c:\xampp\bin\php
c:\> cd c:\xampp\bin\php
c:\> (echo @ECHO OFF & echo php "%~dp0symfony" %*) > symfony.bat

c:\> copy symfony c:\projects
c:\> cd projects
```


* [XAMPP](https://www.apachefriends.org/index.html) - install and make sure to configure and start MySQL module. 

While in desired directory (e.g. c:\projects) clone the repository
```
git clone https://mblaskovic@bitbucket.org/mblaskovic/shop-test-app.git
```

Navigate to cloned repository directory, create database, entities and admin account
```
cd shop-test-app
php app/console doctrine:database:create 		
php app/console doctrine:schema:update --force
php app/console fos:user:create --super-admin
```


## Running the application

While in cloned repository directory start the server
```
php app/console server:run
```
and make sure to start MySQL XAMPP module


In your browser navigate to
```
http://localhost:8000/home
```
and you can start using the application


## Built With

* [Symfony](https://symfony.com/) - The web framework used


## Authors

* **Mihovil Blašković**   - [mblaskovic](https://bitbucket.org/mblaskovic/)

