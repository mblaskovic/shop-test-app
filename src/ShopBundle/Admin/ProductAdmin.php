<?php
namespace ShopBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('code', 'number')
            ->add('categoryId', 'entity', array(
                'class' => 'ShopBundle\Entity\Category',
                'property' => 'name',
            ));

            //->add('name', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('code')
            ->add('categoryId', null, array(), 'entity', array(
                'class'    => 'ShopBundle\Entity\Category',
                'choice_label' => 'name',
            ));


    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('code')
            ->addIdentifier('categoryId.name');

    }



}