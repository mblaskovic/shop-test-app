<?php

namespace ShopBundle\Controller;

use ShopBundle\Entity\Category;
use ShopBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;

class CategoryController extends Controller
{

    public function postAction(Request $request)
    {

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category );
        $form -> handleRequest($request);


        $em = $this->getDoctrine()->getManager();

        $name = $request->get('name');

        $category->setName($name);

        $em->persist($category);
        $em->flush();

        $this->addFlash('success','Record added!');

        return new Response('Record added!');

    }

    public function listAction()
    {
        $records = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();


        return $this->render('@Shop/Category/list.html.twig', array(
            'records'=>$records,
        ));
    }


    public function addAction(Request $request)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category );
        $form -> handleRequest($request);

        if ($form->isValid() && $form->get('save')->isClicked()) {

            $name = $form['name']->getData();

            $em = $this->getDoctrine()->getManager();

            $category->setName($name);

            $em->persist($category);
            $em->flush();

            $this->addFlash('success','Record added!');

        }

        $widget = $this->get('message.widget');
        return $this->render('ShopBundle:Category:add.html.twig', array(
            'form'=>$form->createView(), 'widget'=>$widget
        ));
    }

    public function editAction(Request $request, Category $category)
    {

        $form = $this->createForm(CategoryType::class, $category );
        $form -> handleRequest($request);

        if ($form->isValid() && $form->get('save')->isClicked()) {


            $em = $this->getDoctrine()->getManager();

            $em->persist($category);
            $em->flush();

            $this->addFlash('success','Record changed!');

        }


        if ($form->get('delete')->isClicked()) {
            return $this->redirectToRoute("category_delete", array('id' => $category->getId()));
        }

        return $this->render('ShopBundle:Category:edit.html.twig', array(
            'form'=>$form->createView()
        ));



    }

    public function deleteAction( Category $category)
    {

        $record = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy(array('id' => $category));


        $em = $this->getDoctrine()->getManager();

        try{
            $em->remove($record);
            $em->flush();


            return $this->redirectToRoute("categories");
        }
        catch (\Exception $e) {
            $error = ("Caught exception: \n".  $e->getMessage(). "\n");
            return new Response($error);
        }




    }

}
