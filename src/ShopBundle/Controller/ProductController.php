<?php

namespace ShopBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ShopBundle\Entity\Category;
use ShopBundle\Entity\Product;
use ShopBundle\Form\ProductType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use ShopBundle\Event\EditEvent;

class ProductController extends Controller
{


    public function postAction(Request $request)
    {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product );
        $form -> handleRequest($request);



        $em = $this->getDoctrine()->getManager();

        $name = $request->get('name');
        $code = $request->get('code');
        $category_id = $request->get('category_id');
        $product->setName($name);
        $product->setCode($code);

        $record = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy(array('id' => $category_id));
        if ($record){
            $product->setCategoryId($record);
        }
        else{
            return new Response('Error! No category with id '.$category_id);
        }

        $em->persist($product);
        $em->flush();

        $this->addFlash('success','Record added!');

        return new Response('Record added!');

    }



    public function addAction(Request $request)
    {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product );
        $form -> handleRequest($request);

        if ($form->isValid() && $form->get('save')->isClicked()) {


        $em = $this->getDoctrine()->getManager();

        $em->persist($product);
        $em->flush();

        $this->addFlash('success','Record added!');
        return $this->redirectToRoute("products");

        }
        $widget = $this->get('message.widget');

        return $this->render('ShopBundle:Product:add.html.twig', array(
            'form'=>$form->createView(),'widget'=>$widget
        ));
    }

    /**
     * @ParamConverter("product", class="ShopBundle:Product")
     */

    public function editAction(Request $request,  $product)
    {



        $form = $this->createForm(ProductType::class, $product );
        $form -> handleRequest($request);

        if ($form->isValid() && $form->get('save')->isClicked()) {



            $em = $this->getDoctrine()->getManager();


            $em->persist($product);
            $em->flush();

            $this->addFlash('success','Record changed!');

            $eventDispatcher = $this->get('event_dispatcher');
            $event = new EditEvent();
            $message=$eventDispatcher->dispatch('edit_event', $event);


            $logger = $this->get('monolog.logger.edit_event_logger');
            $logger->info($message->getMessage());

        }



        if ($form->get('delete')->isClicked()) {
            return $this->redirectToRoute("products_delete", array('id' => $product->getId()));
        }

        return $this->render('ShopBundle:Product:edit.html.twig', array(
            'form'=>$form->createView()
        ));

    }



    public function deleteAction( Product $product)
    {

        $record = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy(array('id' => $product));


        $em = $this->getDoctrine()->getManager();
        $em->remove($record);
        $em->flush();
        $this->addFlash('danger','Record deleted!');

        return $this->redirectToRoute("products");


    }


    public function listAction()
    {

        $records = $this->getDoctrine()
            ->getRepository(Product::class)
            ->getProductsOrderByNameAsc();

        return $this->render('ShopBundle:Product:list.html.twig', array(
            'records'=>$records
        ));
    }




}
