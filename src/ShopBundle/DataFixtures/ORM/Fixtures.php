<?php
namespace ShopBundle\DataFixtures\ORM;

use ShopBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 5; $i++) {
            $category = new Category();
            $category->setName('category '.$i);

            $manager->persist($category);
        }

        $manager->flush();
    }
}