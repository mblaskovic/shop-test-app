<?php

namespace ShopBundle\Event;
use Symfony\Component\EventDispatcher\Event;


class EditEvent extends Event
{


    public function getMessage()
    {
        $date = date('d-m-Y H:i:s');

        $message='RECORD CHANGED at '.$date;
        return $message;
    }
}