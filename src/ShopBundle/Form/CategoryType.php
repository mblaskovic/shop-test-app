<?php

namespace ShopBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array
            ('attr' => array('style'=> 'margin-bottom:15px;margin-left:15px')))

            ->add('save',SubmitType::class, array('label'=>'Submit',
                'attr' => array('class'=> 'btn btn-primary', 'style'=>'margin-bottom:15px', 'display'=>'flex')))

            ->add('delete',SubmitType::class, array('label'=>'Delete',
                'attr' => array('class'=> 'btn btn-danger', 'style'=>'margin-bottom:15px',
                    'display'=>'flex', 'onclick' => 'return confirm("Are you sure you want to delete this record?")'  )))


        ;

    }
}
